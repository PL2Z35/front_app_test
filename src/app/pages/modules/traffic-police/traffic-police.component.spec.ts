import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TrafficPoliceComponent } from './traffic-police.component';
import { RouterTestingModule } from '@angular/router/testing';

describe('TrafficPoliceComponent', () => {
  let component: TrafficPoliceComponent;
  let fixture: ComponentFixture<TrafficPoliceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule],
      declarations: [TrafficPoliceComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(TrafficPoliceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
