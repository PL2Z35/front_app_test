import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { tap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { History } from '../../../../common/interfaces/history.interface';
import { TrafficPoliceService } from '../services/traffic-police.service';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css'],
})
export class HistoryComponent implements OnInit {
  historys!: History[];

  constructor(
    private trafficPoliceService: TrafficPoliceService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(params => {
      this.trafficPoliceService
        .getHistory(+params['id'])
        .pipe(tap((historys: History[]) => (this.historys = historys)))
        .subscribe();
    });
  }

  getTrafficPolice(): void {
    this.router.navigate(['/trafficPolice']);
  }
}
