import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TrafficPoliceRoutingModule } from './traffic-police-routing.module';
import { TrafficPoliceComponent } from './traffic-police.component';
import { MaterialModule } from 'src/app/material.module';
import { AddComponent } from './add/add.component';
import { FormsModule } from '@angular/forms';
import { EditComponent } from './edit/edit.component';
import { HistoryComponent } from './history/history.component';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    TrafficPoliceComponent,
    AddComponent,
    EditComponent,
    HistoryComponent,
  ],
  imports: [
    CommonModule,
    TrafficPoliceRoutingModule,
    MaterialModule,
    FormsModule,
    HttpClientModule,
  ],
})
export class TrafficPoliceModule {}
