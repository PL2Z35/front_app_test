import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddComponent } from './add/add.component';
import { EditComponent } from './edit/edit.component';
import { HistoryComponent } from './history/history.component';
import { TrafficPoliceComponent } from './traffic-police.component';

const routes: Routes = [
  { path: '', component: TrafficPoliceComponent },
  { path: 'add', component: AddComponent },
  { path: 'edit/:id', component: EditComponent },
  { path: ':id/history', component: HistoryComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TrafficPoliceRoutingModule {}
