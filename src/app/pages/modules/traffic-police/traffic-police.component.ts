import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { tap } from 'rxjs';
import { TrafficPolice } from '../../../common/interfaces/traffic-police.interface';
import { TrafficPoliceService } from './services/traffic-police.service';

@Component({
  selector: 'app-traffic-police',
  templateUrl: './traffic-police.component.html',
  styleUrls: ['./traffic-police.component.css'],
})
export class TrafficPoliceComponent implements OnInit {
  trafficPolices: TrafficPolice[] = [];

  constructor(
    private router: Router,
    private trafficPoliceService: TrafficPoliceService
  ) {}

  ngOnInit(): void {
    this.trafficPoliceService
      .getTrafficPolices()
      .pipe(
        tap(
          (trafficPolices: TrafficPolice[]) =>
            (this.trafficPolices = trafficPolices)
        )
      )
      .subscribe();
  }

  getHighway(): void {
    this.router.navigate(['/highway']);
  }

  addTrafficPolice(): void {
    this.router.navigate(['/trafficPolice/add']);
  }

  deleteTrafficPolice(id: number): void {
    this.trafficPoliceService
      .deleteTrafficPolice(id)
      .pipe(tap(res => console.log(res)))
      .subscribe();
    window.location.reload();
  }

  editTrafficPolice(trafficPolice: TrafficPolice): void {
    this.router.navigate(['/trafficPolice/edit', trafficPolice.id]);
  }

  getHistory(id: number): void {
    this.router.navigate(['/trafficPolice/' + id + '/history']);
  }
}
