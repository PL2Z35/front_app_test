import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { AddComponent } from './add.component';

describe('AddComponent', () => {
  let component: AddComponent;
  let fixture: ComponentFixture<AddComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule],
      declarations: [AddComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(AddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call getTrafficPolice()', () => {
    spyOn(component, 'getTrafficPolice');
    component.getTrafficPolice();
    expect(component.getTrafficPolice).toHaveBeenCalled();
  });

  it('should call getAddTrafficPolice()', () => {
    spyOn(component, 'getAddTrafficPolice');
    component.getAddTrafficPolice();
    expect(component.getAddTrafficPolice).toHaveBeenCalled();
  });
});
