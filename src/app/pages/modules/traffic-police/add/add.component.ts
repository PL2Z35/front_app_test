import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HighwayService } from '../../highway/services/highway.service';
import { TrafficPolice } from '../../../../common/interfaces/traffic-police.interface';
import { TrafficPoliceService } from '../services/traffic-police.service';
import { tap } from 'rxjs';
import { Highway } from '../../../../common/interfaces/highway.interface';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css'],
})
export class AddComponent implements OnInit {
  constructor(
    private router: Router,
    private trafficPoliceService: TrafficPoliceService,
    private highwayService: HighwayService
  ) {}

  trafficPolice!: TrafficPolice;
  highwaysList: Highway[] = [];

  ngOnInit(): void {
    this.trafficPolice = {
      id: 0,
      name: '',
      yearsExperience: 0,
      idSecretary: '',
      highway: 0,
    };
    this.highwayService
      .getHighwaysAvailable()
      .pipe(tap((highways: Highway[]) => (this.highwaysList = highways)))
      .subscribe();
  }

  getTrafficPolice(): void {
    this.router.navigate(['/trafficPolice']);
  }

  getAddTrafficPolice(): void {
    this.trafficPoliceService
      .addTrafficPolice(this.trafficPolice)
      .subscribe(() => this.getTrafficPolice());
    this.router.navigate(['/trafficPolice']);
  }
}
