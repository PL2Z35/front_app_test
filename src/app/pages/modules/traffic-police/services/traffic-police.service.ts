import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TrafficPolice } from '../../../../common/interfaces/traffic-police.interface';
import { Observable } from 'rxjs/internal/Observable';
import { History } from '../../../../common/interfaces/history.interface';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})

export class TrafficPoliceService {
  apiUrl: string = environment.ApiUrl + '/trafficPolice';

  constructor(private http: HttpClient) {}

  getTrafficPolices(): Observable<TrafficPolice[]> {
    return this.http.get<TrafficPolice[]>(this.apiUrl + '/all');
  }

  addTrafficPolice(trafficPolice: TrafficPolice): Observable<TrafficPolice> {
    return this.http.post<TrafficPolice>(this.apiUrl, trafficPolice);
  }

  deleteTrafficPolice(id: number): Observable<TrafficPolice> {
    return this.http.delete<TrafficPolice>(this.apiUrl + '/' + id);
  }

  updateTrafficPolice(trafficPolice: TrafficPolice): Observable<TrafficPolice> {
    return this.http.put<TrafficPolice>(this.apiUrl, trafficPolice);
  }

  getHistory(id: number): Observable<History[]> {
    return this.http.get<History[]>(this.apiUrl + '/' + id + '/history');
  }

  getTrafficPolice(id: number): Observable<TrafficPolice> {
    return this.http.get<TrafficPolice>(this.apiUrl + '/' + id);
  }
}
