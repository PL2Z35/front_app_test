import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';

import { TrafficPoliceService } from './traffic-police.service';

describe('TrafficPoliceService', () => {
  let service: TrafficPoliceService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    service = TestBed.inject(TrafficPoliceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should call getTrafficPolices()', () => {
    spyOn(service, 'getTrafficPolices');
    service.getTrafficPolices();
    expect(service.getTrafficPolices).toHaveBeenCalled();
  });

  it('should call addTrafficPolice()', () => {
    const trafficPolice = {
      id: 23,
      name: '',
      yearsExperience: 0,
      idSecretary: ',',
      highway: 23,
    };
    spyOn(service, 'addTrafficPolice');
    service.addTrafficPolice(trafficPolice);
    expect(service.addTrafficPolice).toHaveBeenCalled();
  });

  it('should call getTrafficPolice()', () => {
    spyOn(service, 'getTrafficPolice');
    service.getTrafficPolice(23);
    expect(service.getTrafficPolice).toHaveBeenCalled();
  });

  it('should call updateTrafficPolice()', () => {
    const trafficPolice = {
      id: 23,
      name: '',
      yearsExperience: 0,
      idSecretary: ',',
      highway: 23,
    };
    spyOn(service, 'updateTrafficPolice');
    service.updateTrafficPolice(trafficPolice);
    expect(service.updateTrafficPolice).toHaveBeenCalled();
  });

  it('should call getHistory()', () => {
    spyOn(service, 'getHistory');
    service.getHistory(23);
    expect(service.getHistory).toHaveBeenCalled();
  });

  it('should call deleteTrafficPolice()', () => {
    spyOn(service, 'deleteTrafficPolice');
    service.deleteTrafficPolice(23);
    expect(service.deleteTrafficPolice).toHaveBeenCalled();
  });
});
