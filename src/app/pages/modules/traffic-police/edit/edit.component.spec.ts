import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { EditComponent } from './edit.component';

describe('EditComponent', () => {
  let component: EditComponent;
  let fixture: ComponentFixture<EditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule],
      declarations: [EditComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(EditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call getTrafficPolice()', () => {
    spyOn(component, 'getTrafficPolice');
    component.getTrafficPolice();
    expect(component.getTrafficPolice).toHaveBeenCalled();
  });

  it('should call getUpdateTrafficPolice()', () => {
    spyOn(component, 'getUpdateTrafficPolice');
    component.getUpdateTrafficPolice();
    expect(component.getUpdateTrafficPolice).toHaveBeenCalled();
  });
});
