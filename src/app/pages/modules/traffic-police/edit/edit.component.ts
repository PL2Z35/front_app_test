import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { tap } from 'rxjs/operators';
import { Highway } from '../../../../common/interfaces/highway.interface';
import { HighwayService } from '../../highway/services/highway.service';
import { TrafficPolice } from '../../../../common/interfaces/traffic-police.interface';
import { TrafficPoliceService } from '../services/traffic-police.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css'],
})
export class EditComponent implements OnInit {
  trafficPolice: TrafficPolice = {
    id: 0,
    name: '',
    yearsExperience: 0,
    idSecretary: '',
    highway: 0,
  };
  highwaysList: Highway[] = [];

  constructor(
    private router: Router,
    private activeRoute: ActivatedRoute,
    private trafficPoliceService: TrafficPoliceService,
    private highwayService: HighwayService
  ) {}

  ngOnInit(): void {
    this.activeRoute.params.subscribe(params => {
      if (params === undefined) {
        this.router.navigate(['/trafficPolice']);
      } else {
        this.trafficPoliceService
          .getTrafficPolice(+params['id'])
          .pipe(
            tap(
              (trafficPolice: TrafficPolice) =>
                (this.trafficPolice = trafficPolice)
            )
          )
          .subscribe();
      }
    });
    this.highwayService
      .getHighwaysAvailable()
      .pipe(tap((highways: Highway[]) => (this.highwaysList = highways)))
      .subscribe();
  }

  getTrafficPolice(): void {
    this.router.navigate(['/trafficPolice']);
  }

  getUpdateTrafficPolice(): void {
    this.trafficPoliceService
      .updateTrafficPolice(this.trafficPolice)
      .subscribe(() => this.getTrafficPolice());
    this.router.navigate(['/trafficPolice']);
  }
}
