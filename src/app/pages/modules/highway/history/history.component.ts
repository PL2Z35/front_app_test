import { Component, OnInit } from '@angular/core';
import { HighwayService } from '../services/highway.service';
import { ActivatedRoute } from '@angular/router';
import { tap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { History } from '../../../../common/interfaces/history.interface';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css'],
})
export class HistoryComponent implements OnInit {
  historys!: History[];

  constructor(
    private highwayService: HighwayService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(params => {
      this.highwayService
        .getHistory(+params['id'])
        .pipe(tap((historys: History[]) => (this.historys = historys)))
        .subscribe();
    });
  }

  getHighways(): void {
    this.router.navigate(['/highway']);
  }
}
