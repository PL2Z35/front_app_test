import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HighwayService } from '../services/highway.service';
import { tap } from 'rxjs/operators';
import { Highway } from '../../../../common/interfaces/highway.interface';
import { Router } from '@angular/router';
import { HighwayComponent } from '../highway.component';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css'],
})
export class EditComponent implements OnInit {
  @ViewChild(HighwayComponent) highwayComponent: any;
  highway: Highway = {
    id: 0,
    type: '',
    streetOrRace: '',
    number: 0,
    congestionLevel: 0,
  };
  constructor(
    private highwayService: HighwayService,
    private activeRoute: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.activeRoute.params.subscribe(params => {
      if (params === undefined) {
        this.router.navigate(['/highway']);
      } else {
        this.highwayService
          .getHighway(+params['id'])
          .pipe(tap((highway: Highway) => (this.highway = highway)))
          .subscribe();
      }
    });
  }

  getHighways(): void {
    this.router.navigate(['/highway']);
  }

  getUpdateHighway(): void {
    this.highwayService
      .updateHighway(this.highway)
      .subscribe(() => this.getHighways());
    this.router.navigate(['/highway']);
  }
}
