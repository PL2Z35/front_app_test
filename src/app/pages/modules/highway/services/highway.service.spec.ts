import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { HighwayService } from './highway.service';

describe('HighwayService', () => {
  let service: HighwayService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    service = TestBed.inject(HighwayService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should call getHighways()', () => {
    spyOn(service, 'getHighways');
    service.getHighways();
    expect(service.getHighways).toHaveBeenCalled();
  });

  it('should call getAddHighway()', () => {
    const highway = {
      id: 23,
      type: '',
      streetOrRace: '',
      number: 0,
      congestionLevel: 0,
    };
    spyOn(service, 'addHighway');
    service.addHighway(highway);
    expect(service.addHighway).toHaveBeenCalled();
  });

  it('should call getHighway()', () => {
    spyOn(service, 'getHighway');
    service.getHighway(23);
    expect(service.getHighway).toHaveBeenCalled();
  });

  it('should call updateHighway()', () => {
    const highway = {
      id: 23,
      type: '',
      streetOrRace: '',
      number: 0,
      congestionLevel: 0,
    };
    spyOn(service, 'updateHighway');
    service.updateHighway(highway);
    expect(service.updateHighway).toHaveBeenCalled();
  });

  it('should call getHistory()', () => {
    spyOn(service, 'getHistory');
    service.getHistory(23);
    expect(service.getHistory).toHaveBeenCalled();
  });

  it('should call deleteHighway()', () => {
    spyOn(service, 'deleteHighway');
    service.deleteHighway(23);
    expect(service.deleteHighway).toHaveBeenCalled();
  });
});
