import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Highway } from '../../../../common/interfaces/highway.interface';
import { History } from '../../../../common/interfaces/history.interface';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class HighwayService {
  apiUrl: string = environment.ApiUrl + '/highway';

  constructor(private http: HttpClient) {}

  getHighways(): Observable<Highway[]> {
    return this.http.get<Highway[]>(this.apiUrl + '/all');
  }

  getHighway(id: number): Observable<Highway> {
    return this.http.get<Highway>(this.apiUrl + '/' + id);
  }

  getHistory(id: number): Observable<History[]> {
    return this.http.get<History[]>(this.apiUrl + '/' + id + '/history/');
  }

  deleteHighway(id: number): Observable<Highway> {
    return this.http.delete<Highway>(this.apiUrl + '/' + id);
  }

  addHighway(highway: Highway): Observable<Highway> {
    return this.http.post<Highway>(this.apiUrl, highway);
  }

  updateHighway(highway: Highway): Observable<Highway> {
    return this.http.put<Highway>(this.apiUrl, highway);
  }

  getHighwaysAvailable(): Observable<Highway[]> {
    return this.http.get<Highway[]>(this.apiUrl + '/available');
  }
}
