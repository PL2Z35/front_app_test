import { Component, OnInit, Output } from '@angular/core';
import { tap } from 'rxjs/operators';
import { Highway } from '../../../common/interfaces/highway.interface';
import { HighwayService } from './services/highway.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-highway',
  templateUrl: './highway.component.html',
  styleUrls: ['./highway.component.css'],
})
export class HighwayComponent implements OnInit {
  highways!: Highway[];
  highway!: Highway;

  constructor(private highwayService: HighwayService, private router: Router) {}

  ngOnInit(): void {
    this.highwayService
      .getHighways()
      .pipe(tap((highways: Highway[]) => (this.highways = highways)))
      .subscribe();
  }

  getHistory(id: number): void {
    this.router.navigate(['/highway/' + id + '/history']);
  }

  deleteHighway(id: number): void {
    this.highwayService
      .deleteHighway(id)
      .pipe(tap(res => console.log(res)))
      .subscribe();
    window.location.reload();
  }

  updateHighway(highway: Highway): void {
    this.highway = highway;
    this.router.navigate(['/highway/edit/', +highway.id]);
  }

  addHighway(): void {
    this.router.navigate(['/highway/add']);
  }

  getTrafficPolice(): void {
    this.router.navigate(['/trafficPolice']);
  }
}
