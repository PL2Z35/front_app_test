import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HighwayComponent } from './highway.component';

describe('HighwayComponent', () => {
  let component: HighwayComponent;
  let fixture: ComponentFixture<HighwayComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule],
      declarations: [HighwayComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(HighwayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
