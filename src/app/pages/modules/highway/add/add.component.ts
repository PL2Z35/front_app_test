import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Highway } from '../../../../common/interfaces/highway.interface';
import { HighwayService } from '../services/highway.service';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css'],
})
export class AddComponent implements OnInit {
  constructor(private router: Router, private highwayService: HighwayService) {}

  highway!: Highway;

  ngOnInit(): void {
    this.highway = {
      id: 0,
      type: '',
      streetOrRace: '',
      number: 0,
      congestionLevel: 0,
    };
  }

  getHighways(): void {
    this.router.navigate(['/highway']);
  }

  getAddHighway(): void {
    this.highwayService
      .addHighway(this.highway)
      .subscribe(() => this.getHighways());
    this.router.navigate(['/highway']);
  }
}
