import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { AddComponent } from './add.component';

describe('AddComponent', () => {
  let component: AddComponent;
  let fixture: ComponentFixture<AddComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule],
      declarations: [AddComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(AddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call getHighways()', () => {
    spyOn(component, 'getHighways');
    component.getHighways();
    expect(component.getHighways).toHaveBeenCalled();
  });

  it('should call getAddHighway()', () => {
    spyOn(component, 'getAddHighway');
    component.getAddHighway();
    expect(component.getAddHighway).toHaveBeenCalled();
  });
});
