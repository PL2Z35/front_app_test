import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HighwayRoutingModule } from './highway-routing.module';
import { HighwayComponent } from './highway.component';
import { MaterialModule } from 'src/app/material.module';

@NgModule({
  declarations: [HighwayComponent],
  imports: [CommonModule, HighwayRoutingModule, MaterialModule],
})
export class HighwayModule {}
