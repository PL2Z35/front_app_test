import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'trafficPolice',
    loadChildren: () =>
      import('./pages/modules/traffic-police/traffic-police.module').then(
        m => m.TrafficPoliceModule
      ),
  },
  {
    path: 'highway',
    loadChildren: () =>
      import('./pages/modules/highway/highway.module').then(
        m => m.HighwayModule
      ),
  },
  { path: '**', redirectTo: 'highway', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
