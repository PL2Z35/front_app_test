export interface TrafficPolice {
  id: number;
  name: string;
  yearsExperience: number;
  idSecretary: string;
  highway: number;
}
