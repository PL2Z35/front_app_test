export interface Highway {
  id: number;
  type: string;
  streetOrRace: string;
  number: number;
  congestionLevel: number;
}
