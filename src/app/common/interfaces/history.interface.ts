export interface History {
  id: number;
  transitAgentId: number;
  highwayId: number;
  date: string;
}
