FROM node:latest as node
WORKDIR /app
COPY ./ /app/
RUN npm install
RUN npm install -g @angular/cli
RUN npm run-script build --prod

FROM nginx:latest as nginx
COPY --from=node /app/dist/app-test/ /usr/share/nginx/html